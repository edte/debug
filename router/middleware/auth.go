// @program:     debug
// @file:        auth.go
// @author:      edte
// @create:      2021-04-05 17:03
// @description:
package middleware

import (
	"strings"

	"github.com/gin-gonic/gin"

	"debug/router/response"
	"debug/router/service"
)

// JWTAuthMiddleware 基于JWT的认证中间件
func JWTAuthMiddleware() func(c *gin.Context) {
	return func(c *gin.Context) {
		// 客户端携带Token有三种方式 1.放在请求头 2.放在请求体 3.放在URI
		// 这里假设Token放在Header的Authorization中，并使用Bearer开头
		// 这里的具体实现方式要依据你的实际业务情况决定
		authHeader := c.Request.Header.Get("Authorization")
		if authHeader == "" {
			response.Error(c, 1005, "token not found")
			c.Abort()
			return
		}

		// 按空格分割
		parts := strings.SplitN(authHeader, " ", 2)
		if !(len(parts) == 2 && parts[0] == "Bearer") {
			response.Error(c, 1006, "auth header not right")
			c.Abort()
			return
		}

		// parts[1]是获取到的tokenString，我们使用之前定义好的解析JWT的函数来解析它
		mc, err := service.ParseToken(parts[1])
		if err != nil {
			response.Error(c, 1007, "token format not right")
			c.Abort()
			return
		}

		// 将当前请求的 uid 信息保存到请求的上下文c上
		c.Set("uid", mc.Uid)
		c.Next() // 后续的处理函数可以用过c.Get("username")来获取当前请求的用户信息
	}
}
