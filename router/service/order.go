// @program:     debug
// @file:        order.go
// @author:      edte
// @create:      2021-04-05 20:12
// @description:
package service

import (
	"debug/errors"
	"debug/model"
)

type Order struct {
	OrderId string `json:"order_id"`
	TopicID string `json:"topic_id"` // 订单所属分区
	Title   string `json:"title"`    // 订单标题
	Price   string `json:"price"`    // 价格
	Expire  string `json:"expire"`   // ddl
	Content string `json:"content"`  // 内容
}

func AddOrder(s string, o Order) error {
	err := model.AddOrder(model.Order{
		OrderID: NewOrderId(),
		UserID:  s,
		TopicID: o.TopicID,
		Title:   o.Title,
		Price:   o.Price,
		Expire:  o.Expire,
		Content: o.Content,
	})

	if err != nil {
		return errors.WarpRouter("add order failed", err)
	}

	return nil
}

func UpdateOrder() {
	// model.UpdateOrder()
}

func DeleteOrder() {
	// model.DeleteOrder()
}

func SearchOrderByUid(uid string) (o []Order, err error) {
	orders, err := model.SearchOrderByUid(uid)
	if err != nil {
		return nil, errors.WarpRouter("search user's order failed", err)
	}

	for _, order := range orders {
		o = append(o, Order{
			OrderId: order.OrderID,
			TopicID: order.TopicID,
			Title:   order.Title,
			Price:   order.Price,
			Expire:  order.Expire,
			Content: order.Content,
		})
	}

	return
}

type OrderInfo struct {
	OrderID  string `json:"order_id"`
	TopicID  string `json:"topic_id"` // 订单所属分区
	Title    string `json:"title"`    // 订单标题
	Price    string `json:"price"`    // 价格
	Expire   string `json:"expire"`   // ddl
	Content  string `json:"content"`  // 内容
	IsFinish bool   `json:"is_finish"`
	User     struct {
		UserID   string `json:"user_id"`  // 用户 id
		Nickname string `json:"nickname"` // 用户昵称
		Avatar   string `json:"avatar"`   // 用户头像
		TopicID  string `json:"topic_id"`
	} `json:"user"`
}

func GetOrderList(uid string, t string) (orders []OrderInfo, err error) {
	var p []model.Order

	user, err := SearchUser(uid)
	if err != nil {
		return nil, errors.WarpRouter("search user when get order list failed", err)
	}

	if t == "0" {
		p, err = model.SearchOrderByTime()
	} else if t == "1" {
		p, err = model.SearchOrderByTopic(user.TopicID)
	} else {
		return nil, errors.New(errors.Router, "unsupport type")
	}

	if err != nil {
		return nil, errors.WarpRouter("search order by time or topic when get order list failed", err)
	}

	for i, order := range p {
		u, err := SearchUser(p[i].UserID)
		if err != nil {
			return nil, errors.WarpRouter("search user info failed", err)
		}

		orders = append(orders, OrderInfo{
			OrderID: order.OrderID,
			TopicID: order.TopicID,
			Title:   order.Title,
			Price:   order.Price,
			Expire:  order.Expire,
			Content: order.Content,
			User: struct {
				UserID   string `json:"user_id"`
				Nickname string `json:"nickname"`
				Avatar   string `json:"avatar"`
				TopicID  string `json:"topic_id"`
			}{
				UserID:   u.UserID,
				Nickname: u.Nickname,
				Avatar:   u.Avatar,
				TopicID:  u.TopicID,
			},
		})
	}

	return
}

func GetOrderListByTopicID(uid string, topicID string) (orders []OrderInfo, err error) {
	var p []model.Order

	p, err = model.SearchOrderByTopic(topicID)
	if err != nil {
		return nil, errors.WarpRouter("search order by topic failed", err)
	}

	for i, order := range p {
		u, err := SearchUser(p[i].UserID)
		if err != nil {
			return nil, errors.WarpRouter("search user info failed", err)
		}

		orders = append(orders, OrderInfo{
			OrderID: order.OrderID,
			TopicID: order.TopicID,
			Title:   order.Title,
			Price:   order.Price,
			Expire:  order.Expire,
			Content: order.Content,
			User: struct {
				UserID   string `json:"user_id"`
				Nickname string `json:"nickname"`
				Avatar   string `json:"avatar"`
				TopicID  string `json:"topic_id"`
			}{
				UserID:   u.UserID,
				Nickname: u.Nickname,
				Avatar:   u.Avatar,
				TopicID:  u.TopicID,
			},
		})
	}

	return
}

func GetOrderBySearch(uid string, key string, data string) (orders []OrderInfo, err error) {
	var p []model.Order

	if key == "title" {
		p, err = model.SearchOrderByTitle(data)
	} else if key == "content" {
		p, err = model.SearchOrderByContent(data)
	} else {
		return nil, errors.New(errors.Router, "unsupported key type")
	}

	if err != nil {
		return nil, errors.WarpRouter("search order by like failed", err)
	}

	for i, order := range p {
		u, err := SearchUser(p[i].UserID)
		if err != nil {
			return nil, errors.WarpRouter("search user info failed", err)
		}

		orders = append(orders, OrderInfo{
			OrderID: order.OrderID,
			TopicID: order.TopicID,
			Title:   order.Title,
			Price:   order.Price,
			Expire:  order.Expire,
			Content: order.Content,
			User: struct {
				UserID   string `json:"user_id"`
				Nickname string `json:"nickname"`
				Avatar   string `json:"avatar"`
				TopicID  string `json:"topic_id"`
			}{
				UserID:   u.UserID,
				Nickname: u.Nickname,
				Avatar:   u.Avatar,
				TopicID:  u.TopicID,
			},
		})
	}

	return
}

type OrderInformation struct {
	OrderID  string `json:"order_id"`
	TopicID  string `json:"topic_id"` // 订单所属分区
	Title    string `json:"title"`    // 订单标题
	Price    string `json:"price"`    // 价格
	Expire   string `json:"expire"`   // ddl
	Content  string `json:"content"`  // 内容
	IsFinish bool   `json:"is_finish"`
	User     struct {
		UserID   string `json:"user_id"`  // 用户 id
		Nickname string `json:"nickname"` // 用户昵称
		Avatar   string `json:"avatar"`   // 用户头像
		TopicID  string `json:"topic_id"`
	} `json:"user"` // 发布者信息

	Proposer []struct {
		Proposal
	} `json:"proposer"`

	Resolve struct {
		Proposal
	}
}

func SearchOrder(uid string, orderID string) (orders OrderInformation, err error) {
	if !model.ExistOrder(orderID) {
		return OrderInformation{}, errors.New(errors.Router, "order not found")
	}

	order, err := model.SearchOrder(orderID)
	if err != nil {
		return OrderInformation{}, errors.WarpRouter("search order failed", err)
	}

	orders.OrderID = order.OrderID
	orders.TopicID = order.TopicID
	orders.Title = order.Title
	orders.Price = order.Price
	orders.Expire = order.Expire
	orders.Content = order.Content

	user, err := model.SearchUser(order.UserID)
	if err != nil {
		return OrderInformation{}, errors.WarpRouter("search user failed", err)
	}

	orders.User.TopicID = user.TopicID
	orders.User.UserID = user.UserID
	orders.User.Avatar = user.Avatar
	orders.User.Nickname = user.Nickname

	proposals, err := model.SearchProposalByOrderID(orderID)
	if err != nil {
		return OrderInformation{}, errors.WarpRouter("serach proposal by order id failed", err)
	}

	for _, proposal := range proposals {
		orders.Proposer = append(orders.Proposer, struct {
			Proposal
		}{
			Proposal{
				ProposalID:   proposal.ProposalID,
				OrderID:      proposal.OrderID,
				Price:        proposal.Price,
				EstimateTime: proposal.EstimateTime,
			},
		})
	}

	isSucceed, proposalID, err := model.IsSucceedOrder(orderID)
	if err != nil {
		return OrderInformation{}, errors.WarpRouter("search result by order id failed", err)
	}

	if !isSucceed {
		return
	}

	orders.IsFinish = isSucceed
	orders.Resolve.ProposalID = proposalID

	return
}
