// @program:     debug
// @file:        result.go
// @author:      edte
// @create:      2021-04-11 21:18
// @description:
package service

import (
	"debug/errors"
	"debug/model"
)

type Result struct {
	Type       int    `json:"type"`        // 结果类型
	OrderID    string `json:"order_id"`    // 订单 id
	ProposalID string `json:"proposal_id"` // 申请 id
}

func AddResult(result Result, uid string) error {
	if !model.ExistProposal(result.ProposalID) {
		return errors.New(errors.Router, "proposal not exist")
	}

	if !model.IsUsersProposal(uid, result.ProposalID) {
		return errors.New(errors.Router, "proposal not belong this user")
	}

	if !model.AbleResultType(result.Type) {
		return errors.New(errors.Router, "invalid type code")
	}

	err := model.AddResult(model.Result{
		Type:       result.Type,
		ResultID:   NewResultID(),
		OrderID:    result.OrderID,
		ProposalID: result.ProposalID,
	})

	if err != nil {
		return errors.WarpRouter("add result failed", err)
	}

	return nil
}

func UpdateResult() {
	// model.UpdateResult()
}

func DeleteResult() {
	// model.DeleteResult()
}

func SearchResult() {
	// model.SearchResult()
}
