// @program:     debug
// @file:        id.go
// @author:      edte
// @create:      2021-04-05 17:14
// @description:
package service

import (
	"bytes"
	"crypto/rand"
	"fmt"
	"math/big"
	mrand "math/rand"
	"time"
	"unsafe"
)

// RandomNumber 生成 n 个随机 number
func RandomNumber(n int) string {
	var numbers = []byte{1, 2, 3, 4, 5, 7, 8, 9}
	var container string

	length := bytes.NewReader(numbers).Len()

	for i := 1; i <= n; i++ {
		random, err := rand.Int(rand.Reader, big.NewInt(int64(length)))
		if err != nil {

		}
		container += fmt.Sprintf("%d", numbers[random.Int64()])
	}

	return container
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = mrand.NewSource(time.Now().UnixNano())

// RandomString 生成 n 个随机 string
func RandomString(n int) string {
	b := make([]byte, n)

	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return *(*string)(unsafe.Pointer(&b))
}

func NewUid() string {
	return RandomNumber(5)
}

func NewOrderId() string {
	return RandomString(8)
}

func NewProposalId() string {
	return RandomNumber(3) + RandomString(5)
}

func NewFeedbackID() string {
	return RandomString(6)
}

func NewResultID() string {
	return RandomString(7)
}

func NewReviewID() string {
	return RandomNumber(5)
}
