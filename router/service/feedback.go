// @program:     debug
// @file:        feedback.go
// @author:      edte
// @create:      2021-04-05 21:18
// @description:
package service

import (
	"fmt"

	"debug/errors"
	"debug/model"
)

type Feedback struct {
	OrderID string `json:"order_id"` // 反馈的订单
	Type    int    `json:"type"`     // 反馈的类型
	Data    string `json:"data"`     // 反馈的数据
}

func AddFeedback(uid string, f Feedback) error {
	if !model.ExistOrder(f.OrderID) {
		return errors.WarpRouter("order failed when add feedback", fmt.Errorf("order not exist"))
	}

	err := model.AddFeedback(model.Feedback{
		FeedbackID: NewFeedbackID(),
		UserID:     uid,
		OrderID:    f.OrderID,
		Type:       f.Type,
		Data:       f.Data,
	})

	if err != nil {
		return errors.WarpRouter("add feedback failed", err)
	}

	return nil
}

func UpdateFeedback() {
	// model.UpdateFeedback()
}

func DeleteFeedback() {
	// model.DeleteFeedback()
}

func SearchFeedback() {
	// model.SearchFeedback()
}
