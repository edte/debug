// @program:     debug
// @file:        user.go
// @author:      edte
// @create:      2021-04-03 22:03
// @description:
package service

import (
	"debug/errors"
	"debug/model"
)

type User struct {
	UserID   string `json:"user_id"`  // 用户 id
	Nickname string `json:"nickname"` // 用户昵称
	TopicID  string `json:"topic"`    // debug 默认分区
	Avatar   string `json:"avatar"`   // 用户头像
	Username string `json:"username"` // 账号
	Password string `json:"password"` // 密码
	Money    int    `json:"money"`    // money
}

func AddUser(username string, password string) error {
	if model.ExistUser(username) {
		return errors.New(errors.Router, "user is already exist")
	}
	if err := model.AddUser(model.User{UserID: NewUid(), Username: username, Password: password}); err != nil {
		return errors.WarpRouter("add user failed", err)
	}
	return nil
}

func Login(username string, password string) (string, error) {
	if !model.ExistUser(username) {
		return "", errors.New(errors.Router, "user is not exist")
	}
	right, err := model.PassWordRight(username, password)
	if err != nil {
		return "", errors.WarpRouter("judge password right failed", err)
	}
	if !right {
		return "", errors.New(errors.Router, "password not right")
	}

	u, err := model.SearchUserByUsername(username)
	if err != nil {
		return "", errors.WarpRouter("search user by username failed", err)
	}

	token, err := GenToken(u.UserID)
	if err != nil {
		return "", errors.WarpRouter("generate token failed", err)
	}

	return token, nil
}

func SearchUser(uid string) (u User, err error) {
	user, err := model.SearchUser(uid)
	if err != nil {
		return User{}, errors.WarpRouter("failed search user", err)
	}

	u = User{
		UserID:   user.UserID,
		Nickname: user.Nickname,
		TopicID:  user.TopicID,
		Avatar:   user.Avatar,
		Username: user.Username,
		Password: user.Password,
		Money:    *user.Money,
	}

	return
}

func UpdateUser(uid string, u User) error {
	return model.UpdateUser(uid, model.User{
		Nickname: u.Nickname,
		TopicID:  u.TopicID,
		Avatar:   u.Avatar,
		Username: u.Username,
		Password: u.Password,
		Money:    &u.Money,
	})
}

func GetUserApplyOrder(uid string) (orders []OrderInfo, err error) {
	var p []model.Proposal

	// 先在 proposal 中找到用户发布的 order id
	p, err = model.SearchProposalByUid(uid)
	if err != nil {
		return nil, errors.WarpRouter("failed search proposal by uid failed", err)
	}

	// 然后遍历 order id
	for _, pr := range p {
		// 找到对应 order 的信息
		order, err := model.SearchOrder(pr.OrderID)
		if err != nil {
			return nil, errors.WarpRouter("failed search order failed", err)
		}

		// 再找到发布 order 的发布者信息
		u, err := SearchUser(order.UserID)
		if err != nil {
			return nil, errors.WarpRouter("search user info failed", err)
		}

		orders = append(orders, OrderInfo{
			TopicID: order.TopicID,
			Title:   order.Title,
			Price:   order.Price,
			Expire:  order.Expire,
			Content: order.Content,
			User: struct {
				UserID   string `json:"user_id"`
				Nickname string `json:"nickname"`
				Avatar   string `json:"avatar"`
				TopicID  string `json:"topic_id"`
			}{
				UserID:   u.UserID,
				Nickname: u.Nickname,
				Avatar:   u.Avatar,
				TopicID:  u.TopicID,
			},
		})
	}

	return
}

func GetUserOrderInfo(uid string, t int) (orders []Order, err error) {
	o, err := SearchOrderByUid(uid)
	if err != nil {
		return nil, errors.WarpRouter("get user's order info failed", err)
	}

	for _, order := range o {
		result, err := model.SearchResultByOrderID(order.OrderId)
		if err != nil {
			return nil, errors.WarpRouter("search result by order id failed", err)
		}
		if result == t {
			orders = append(orders, order)
		}
	}

	return
}
