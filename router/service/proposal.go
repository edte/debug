// @program:     debug
// @file:        proposal.go
// @author:      edte
// @create:      2021-04-05 20:58
// @description:
package service

import (
	"fmt"

	"debug/errors"
	"debug/model"
)

type Proposal struct {
	UserID       string  `json:"user_id"`
	ProposalID   string  `json:"proposal_id"`
	OrderID      string  `json:"order_id"`      // 订单
	Price        float64 `json:"price"`         // 价格
	EstimateTime string  `json:"estimate_time"` // 预计完成时间
}

func AddProposal(uid string, p Proposal) error {
	if !model.ExistOrder(p.OrderID) {
		return errors.WarpRouter("order failed when add proposal", fmt.Errorf("order not exist"))
	}

	pid := NewProposalId()

	err := model.AddProposal(model.Proposal{
		ProposalID:   pid,
		UserID:       uid,
		OrderID:      p.OrderID,
		Price:        p.Price,
		EstimateTime: p.EstimateTime,
	})

	if err != nil {
		return errors.WarpRouter("add proposal failed", err)
	}

	err = model.AddResult(model.Result{
		Type:       model.Accepting,
		OrderID:    p.OrderID,
		ProposalID: pid,
	})

	if err != nil {
		return errors.WarpRouter("add result failed when add proposal", err)
	}

	return nil
}

func UpdateProposal() {
	// model.UpdateProposal()
}

func DeleteProposal() {
	// model.DeleteProposal()
}

func SearchProposal() {
	// model.SearchProposal()
}

func ProposalAgree(uid string, pid string) error {
	if !model.ExistProposal(pid) {
		return errors.New(errors.Router, "proposal not exist")
	}
	result, err := model.SearchResult(pid)
	if err != nil {
		return errors.WarpRouter("search result failed when agree proposal", err)
	}

	if result == model.Succeed {
		return errors.New(errors.Router, "proposal already succeed")
	} else if result == model.Overdue {
		return errors.New(errors.Router, "proposal already overdue")
	} else if result == model.Accepting {
		return errors.New(errors.Router, "proposal not apply")
	}

	err = model.UpdateResult(model.Result{
		Type:       model.Succeed,
		ProposalID: pid,
	})

	if err != nil {
		return errors.WarpRouter("update result failed when agree proposal", err)
	}

	return nil
}

func ProposalApplyAgree(uid string, pid string) error {
	if !model.ExistProposal(pid) {
		return errors.New(errors.Router, "proposal not exist")
	}

	result, err := model.SearchResult(pid)
	if err != nil {
		return errors.WarpRouter("search result failed when apply agree proposal", err)
	}

	if result == model.Succeed {
		return errors.New(errors.Router, "proposal already succeed")
	} else if result == model.Overdue {
		return errors.New(errors.Router, "proposal already overdue")
	} else if result == model.Applyed {
		return errors.New(errors.Router, "proposal already apply")
	}

	err = model.UpdateResult(model.Result{
		Type:       model.Applyed,
		ProposalID: pid,
	})

	if err != nil {
		return errors.WarpRouter("update result failed when apply proposal", err)
	}

	return nil
}
