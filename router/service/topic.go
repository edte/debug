// @program:     debug
// @file:        topic.go
// @author:      edte
// @create:      2021-04-05 20:34
// @description:
package service

type Topic struct {
	Name    string `json:"name"`
	TopicId string `json:"topic_id"`
}

var (
	topics = []Topic{
		{
			Name:    "Android",
			TopicId: "234921",
		},
		{
			Name:    "IOS",
			TopicId: "377891",
		},
		{
			Name:    "OPS",
			TopicId: "638192",
		},
		{
			Name:    "GO",
			TopicId: "982512",
		},
		{
			Name:    "C",
			TopicId: "861932",
		},
		{
			Name:    "PYTHON",
			TopicId: "359173",
		},
		{
			Name:    "PHP",
			TopicId: "876215",
		},
		{
			Name:    "C++",
			TopicId: "987612",
		},
		{
			Name:    "前端",
			TopicId: "251873",
		},
		{
			Name:    "后端",
			TopicId: "915235",
		},
	}
)

func AddTopic() {
	// model.AddTopic()

}

func UpdateTopic() {
	// model.UpdateTopic()
}

func DeleteTopic() {
	// model.DeleteTopic()
}

func SearchTopic() []Topic {
	// model.SearchTopic()

	return topics
}
