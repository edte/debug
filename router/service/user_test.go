// @program:     debug
// @file:        user_test.go.go
// @author:      edte
// @create:      2021-04-12 20:15
// @description:
package service

import (
	"reflect"
	"testing"
)

func TestGetUserOrderInfo(t *testing.T) {
	type args struct {
		uid string
		t   int
	}
	tests := []struct {
		name       string
		args       args
		wantOrders []Order
		wantErr    bool
	}{
		{
			name: "",
			args: args{
				uid: "2341234",
				t:   0,
			},
			wantOrders: nil,
			wantErr:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotOrders, err := GetUserOrderInfo(tt.args.uid, tt.args.t)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetUserOrderInfo() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotOrders, tt.wantOrders) {
				t.Errorf("GetUserOrderInfo() gotOrders = %v, want %v", gotOrders, tt.wantOrders)
			}
		})
	}
}
