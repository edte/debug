// @program:     debug
// @file:        review.go
// @author:      edte
// @create:      2021-04-11 21:52
// @description:
package service

import (
	"debug/errors"
	"debug/model"
)

type Review struct {
	ProposalID string `json:"proposal_id"` // 申请 id
	Data       string `json:"data"`        // 评价的数据
	UserID     string `json:"user_id"`
}

func AddReview(uid string, r Review) error {
	if !model.ExistProposal(r.ProposalID) {
		return errors.New(errors.Router, "proposal not exist")
	}

	if !model.IsUsersProposal(uid, r.ProposalID) {
		return errors.New(errors.Router, "proposal not belong this user")
	}

	if model.ExistReview(r.ProposalID) {
		return errors.New(errors.Router, "proposal already added review")
	}

	err := model.AddReview(model.Review{
		ReviewID:       NewReviewID(),
		UserID:         uid,
		ReviewedUserID: r.UserID,
		ProposalID:     r.ProposalID,
		Data:           r.Data,
	})

	if err != nil {
		return errors.WarpRouter("add review failed", err)
	}

	return nil
}

func UpdateReview() {
	// model.UpdateReview()
}

func DeleteReview() {
	// model.DeleteReview()
}

func SearchReview(uid string) (int, int, error) {
	var (
		data []model.Review
		err  error
	)

	if data, err = model.SearchReview(uid); err != nil {
		return 0, 0, errors.WarpRouter("search review failed", err)
	}

	up := 0
	down := 0

	for _, v := range data {
		if v.Data == "1" {
			up++
		} else if v.Data == "0" {
			down++
		}
	}

	return up, down, nil
}
