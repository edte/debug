// @program:     debug
// @file:        jwt.go
// @author:      edte
// @create:      2021-04-05 16:48
// @description:
package service

import (
	"time"

	"gopkg.in/dgrijalva/jwt-go.v3"

	"debug/errors"
)

// MyClaims 自定义声明结构体并内嵌 jwt.StandardClaims
// jwt包自带的 jwt.StandardClaims 只包含了官方字段
// 我们这里需要额外记录一个 uid 字段，所以要自定义结构体
// 如果想要保存更多信息，都可以添加到这个结构体中
type MyClaims struct {
	Uid string `json:"uid"`
	jwt.StandardClaims
}

var MySecret = []byte("debug")

const TokenExpireDuration = time.Hour * 999999

// GenToken 生成JWT
func GenToken(uid string) (string, error) {
	// 创建一个我们自己的声明
	c := MyClaims{
		uid, // 自定义字段
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(TokenExpireDuration).Unix(), // 过期时间
			Issuer:    "edte",                                     // 签发人
		},
	}
	// 使用指定的签名方法创建签名对象
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, c)
	// 使用指定的secret签名并获得完整的编码后的字符串token
	return token.SignedString(MySecret)
}

// ParseToken 解析 JWT
func ParseToken(tokenString string) (*MyClaims, error) {
	// 解析token
	token, err := jwt.ParseWithClaims(tokenString, &MyClaims{}, func(token *jwt.Token) (i interface{}, err error) {
		return MySecret, nil
	})
	if err != nil {
		return nil, err
	}
	if claims, ok := token.Claims.(*MyClaims); ok && token.Valid { // 校验token
		return claims, nil
	}

	return nil, errors.New(errors.Router, "invalid token")
}
