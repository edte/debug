// @program:     debug
// @file:        response.go
// @author:      edte
// @create:      2021-04-03 19:56
// @description:
package response

import (
	"github.com/gin-gonic/gin"

	"net/http"
)

// 业务码
const (
	CodeOk        = iota + 1000 // 正常响应
	CodeFormError               // 请求表单错误
	CodeError

	DataOk        = "ok"
	DataFormError = "request form errors!"
)

// Ok
func Ok(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"code": CodeOk})
}

// FormError
func FormError(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"code": CodeFormError, "message": DataFormError})
}

// OkWithData
func OkWithData(c *gin.Context, data interface{}) {
	c.JSON(http.StatusOK, gin.H{"code": CodeOk, "message": DataOk, "data": data})
}

// Error
func Error(c *gin.Context, code int, msg string) {
	c.JSON(http.StatusOK, gin.H{"code": code, "message": msg})
}

func Raw(c *gin.Context, d interface{}) {
	c.JSON(http.StatusOK, d)
}
