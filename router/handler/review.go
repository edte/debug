// @program:     debug
// @file:        review.go
// @author:      edte
// @create:      2021-04-11 21:52
// @description:
package handler

import (
	"github.com/gin-gonic/gin"

	"debug/router/response"
	"debug/router/service"
)

func AddReview(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	var f service.Review
	if err := c.ShouldBindJSON(&f); err != nil {
		response.FormError(c)
		return
	}

	if err := service.AddReview(uid.(string), f); err != nil {
		response.Error(c, 10019, err.Error())
		return
	}

	response.Ok(c)
}

func UpdateReview(c *gin.Context) {
	service.UpdateReview()
}

func DeleteReview(c *gin.Context) {
	service.DeleteReview()
}

func SearchReview(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	up, down, err := service.SearchReview(uid.(string))
	if err != nil {
		response.Error(c, 10019, err.Error())
		return
	}

	response.OkWithData(c, gin.H{
		"up":   up,
		"down": down,
	})
}
