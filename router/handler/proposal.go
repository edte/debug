// @program:     debug
// @file:        proposal.go
// @author:      edte
// @create:      2021-04-05 20:58
// @description:
package handler

import (
	"github.com/gin-gonic/gin"

	"debug/router/response"
	"debug/router/service"
)

func AddProposal(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	var p service.Proposal
	if err := c.ShouldBindJSON(&p); err != nil {
		response.FormError(c)
		return
	}

	if err := service.AddProposal(uid.(string), p); err != nil {
		response.Error(c, 10016, err.Error())
		return
	}

	response.Ok(c)
}

func UpdateProposal(c *gin.Context) {
	service.UpdateProposal()
}

func DeleteProposal(c *gin.Context) {
	service.DeleteProposal()
}

func SearchProposal(c *gin.Context) {
	service.SearchProposal()
}

func ProposalAgree(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
		return
	}

	var a Apply
	if err := c.ShouldBindJSON(&a); err != nil {
		response.FormError(c)
		return
	}

	if err := service.ProposalAgree(uid.(string), a.ProposalID); err != nil {
		response.Error(c, 10021, err.Error())
		return
	}

	response.Ok(c)

}

type Apply struct {
	ProposalID string `json:"proposal_id"`
}

func ProposalApplyAgree(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
		return
	}

	var a Apply
	if err := c.ShouldBindJSON(&a); err != nil {
		response.FormError(c)
		return
	}

	if err := service.ProposalApplyAgree(uid.(string), a.ProposalID); err != nil {
		response.Error(c, 10021, err.Error())
		return
	}

	response.Ok(c)
}
