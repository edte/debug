// @program:     debug
// @file:        result.go
// @author:      edte
// @create:      2021-04-11 21:18
// @description:
package handler

import (
	"github.com/gin-gonic/gin"

	"debug/router/response"
	"debug/router/service"
)

func AddResult(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	var r service.Result
	if err := c.ShouldBindJSON(&r); err != nil {
		response.FormError(c)
		return
	}

	if err := service.AddResult(r, uid.(string)); err != nil {
		response.Error(c, 10016, err.Error())
		return
	}

	response.Ok(c)
}

func UpdateResult(c *gin.Context) {
	// service.UpdateResult()
}

func DeleteResult(c *gin.Context) {
	// service.DeleteResult()
}

func SearchResult(c *gin.Context) {
	// service.SearchResult()
}
