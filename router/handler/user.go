// @program:     debug
// @file:        user.go
// @author:      edte
// @create:      2021-04-03 22:02
// @description:
package handler

import (
	"strconv"

	"github.com/gin-gonic/gin"

	"debug/router/response"
	"debug/router/service"
)

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func Register(c *gin.Context) {
	var u User
	if err := c.ShouldBindJSON(&u); err != nil {
		response.FormError(c)
		return
	}

	if err := service.AddUser(u.Username, u.Password); err != nil {
		response.Error(c, response.CodeError, err.Error())
		return
	}

	response.Ok(c)
}

func Login(c *gin.Context) {
	var u User
	if err := c.ShouldBindJSON(&u); err != nil {
		response.FormError(c)
		return
	}

	token, err := service.Login(u.Username, u.Password)
	if err != nil {
		response.Error(c, 1008, err.Error())
		return
	}

	response.OkWithData(c, gin.H{"token": token})
}

func UpdateUser(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	var u service.User
	if err := c.ShouldBindJSON(&u); err != nil {
		response.FormError(c)
		return
	}

	if err := service.UpdateUser(uid.(string), u); err != nil {
		response.Error(c, 10011, "failed to update usr info")
		return
	}

	response.Ok(c)
}

func SearchUser(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	user, err := service.SearchUser(uid.(string))
	if err != nil {
		response.Error(c, 10010, err.Error())
		return
	}

	response.OkWithData(c, user)
}

func GetUserApplyOrder(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	order, err := service.GetUserApplyOrder(uid.(string))
	if err != nil {
		response.Error(c, 10015, err.Error())
		return
	}

	response.OkWithData(c, order)
}

func GetUserOrderInfo(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	t := c.Query("type")

	atoi, err := strconv.Atoi(t)
	if err != nil {
		response.Error(c, 10019, "type not right")
		return
	}

	order, err := service.GetUserOrderInfo(uid.(string), atoi)
	if err != nil {
		response.Error(c, 10015, err.Error())
		return
	}

	response.OkWithData(c, order)
}
