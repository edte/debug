// @program:     debug
// @file:        order.go
// @author:      edte
// @create:      2021-04-05 20:12
// @description:
package handler

import (
	"github.com/gin-gonic/gin"

	"debug/router/response"
	"debug/router/service"
)

func AddOrder(c *gin.Context) {
	var o service.Order
	if err := c.ShouldBindJSON(&o); err != nil {
		response.FormError(c)
		return
	}

	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	if err := service.AddOrder(uid.(string), o); err != nil {
		response.Error(c, 10013, err.Error())
	}

	response.Ok(c)
}

func UpdateOrder(c *gin.Context) {
	service.UpdateOrder()
}

func DeleteOrder(c *gin.Context) {
	service.DeleteOrder()
}

func SearchOrderByUid(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	order, err := service.SearchOrderByUid(uid.(string))
	if err != nil {
		response.Error(c, 10015, err.Error())
		return
	}

	response.OkWithData(c, order)
}

func GetOrderList(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	t := c.Query("type")

	order, err := service.GetOrderList(uid.(string), t)
	if err != nil {
		response.Error(c, 10015, err.Error())
		return
	}

	response.OkWithData(c, order)
}

func GetOrderListByTopicID(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	t := c.Query("id")

	order, err := service.GetOrderListByTopicID(uid.(string), t)
	if err != nil {
		response.Error(c, 10015, err.Error())
		return
	}

	response.OkWithData(c, order)
}

func GetOrderBySearch(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	key := c.Query("key")
	data := c.Query("data")

	order, err := service.GetOrderBySearch(uid.(string), key, data)
	if err != nil {
		response.Error(c, 10015, err.Error())
		return
	}

	response.OkWithData(c, order)
}

func SearchOrder(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	id := c.Query("id")

	order, err := service.SearchOrder(uid.(string), id)
	if err != nil {
		response.Error(c, 10015, err.Error())
		return
	}

	response.OkWithData(c, order)
}
