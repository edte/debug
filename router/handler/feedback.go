// @program:     debug
// @file:        feedback.go
// @author:      edte
// @create:      2021-04-05 21:18
// @description:
package handler

import (
	"github.com/gin-gonic/gin"

	"debug/router/response"
	"debug/router/service"
)

func AddFeedback(c *gin.Context) {
	uid, exists := c.Get("uid")
	if !exists {
		response.Error(c, 10010, "get uid by token failed")
	}

	var f service.Feedback
	if err := c.ShouldBindJSON(&f); err != nil {
		response.FormError(c)
		return
	}

	if err := service.AddFeedback(uid.(string), f); err != nil {
		response.Error(c, 10019, err.Error())
		return
	}

	response.Ok(c)
}

func UpdateFeedback(c *gin.Context) {
	service.UpdateFeedback()
}

func DeleteFeedback(c *gin.Context) {
	service.DeleteFeedback()
}

func SearchFeedback(c *gin.Context) {
	service.SearchFeedback()
}
