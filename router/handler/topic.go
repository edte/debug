// @program:     debug
// @file:        topic.go
// @author:      edte
// @create:      2021-04-05 20:34
// @description:
package handler

import (
	"github.com/gin-gonic/gin"

	"debug/router/response"
	"debug/router/service"
)

func AddTopic(c *gin.Context) {
	service.AddTopic()
}

func UpdateTopic(c *gin.Context) {
	service.UpdateTopic()
}

func DeleteTopic(c *gin.Context) {
	service.DeleteTopic()
}

func SearchTopic(c *gin.Context) {
	topics := service.SearchTopic()
	response.OkWithData(c, topics)
}
