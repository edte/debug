// @program:     debug
// @file:        init.go
// @author:      edte
// @create:      2021-04-03 19:26
// @description:
package router

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	"debug/router/handler"
	"debug/router/middleware"
)

const (
	// user management
	pathLogin   = "/login"
	methodLogin = "POST"

	pathRegister   = "/register"
	methodRegister = "POST"

	pathUserInfo      = "/user/info"
	methodUserInfoGet = "GET"
	methodUserInfoSet = "POST"

	pathUserOrder         = "/user/order"
	methodUserOrderSearch = "GET"

	pathUserOrderApply   = "/user/order/apply"
	methodUserOrderApply = "GET"

	pathUserOrderInfo   = "/user/order/info"
	methodUserOrderInfo = "GET"

	// topic management
	pathTopic         = "/topic"
	methodTopicAdd    = "POST"
	methodTopicDelete = "DELETE"
	methodTopicUpdate = "PUT"
	methodTopicSearch = "GET"

	// order management
	pathOrder         = "/order"
	methodOrderAdd    = "POST"
	methodOrderDelete = "DELETE"
	methodOrderUpdate = "PUT"
	methodOrderSearch = "GET"

	pathOrderList   = "/order/list"
	methodOrderList = "GET"

	pathOrderTopic       = "/order/topic"
	methodOrderTopicList = "GET"

	pathOrderBySearch   = "/order/search"
	methodOrderBySearch = "GET"

	// proposal management
	pathProposal      = "/proposal"
	methodProposalAdd = "POST"

	pathProposalAgree   = "/proposal/agree"
	methodProposalAgree = "POST"

	pathProposalApplyAgree   = "/proposal/apply"
	methodProposalApplyAgree = "POST"

	methodProposalDelete = "DELETE"
	methodProposalUpdate = "PUT"
	methodProposalSearch = "GET"

	// feedback management
	pathFeedback         = "/feedback"
	methodFeedbackAdd    = "POST"
	methodFeedbackDelete = "DELETE"
	methodFeedbackUpdate = "PUT"
	methodFeedbackSearch = "GET"

	// review management
	pathReview         = "/review"
	methodReviewAdd    = "POST"
	methodReviewDelete = "DELETE"
	methodReviewUpdate = "PUT"
	methodReviewSearch = "GET"

	// result management
	pathResult         = "/result"
	methodResultAdd    = "POST"
	methodResultDelete = "DELETE"
	methodResultUpdate = "PUT"
	methodResultSearch = "GET"
)

var (
	routes = []struct {
		name       string
		path       string
		method     string
		handler    gin.HandlerFunc
		middleware gin.HandlerFunc
	}{
		{
			name:    "user register",
			path:    pathRegister,
			method:  methodRegister,
			handler: handler.Register,
		},
		{
			name:    "user login",
			path:    pathLogin,
			method:  methodLogin,
			handler: handler.Login,
		},
		{
			name:       "user get info",
			path:       pathUserInfo,
			method:     methodUserInfoGet,
			handler:    handler.SearchUser,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "user set info",
			path:       pathUserInfo,
			method:     methodUserInfoSet,
			handler:    handler.UpdateUser,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "post a order",
			path:       pathOrder,
			method:     methodOrderAdd,
			handler:    handler.AddOrder,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "get user's order",
			path:       pathUserOrder,
			method:     methodUserOrderSearch,
			handler:    handler.SearchOrderByUid,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "get all topics",
			path:       pathTopic,
			method:     methodTopicSearch,
			handler:    handler.SearchTopic,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "add a proposal",
			path:       pathProposal,
			method:     methodProposalAdd,
			handler:    handler.AddProposal,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "agree a proposal",
			path:       pathProposalAgree,
			method:     methodProposalAgree,
			handler:    handler.ProposalAgree,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "apply to agree a proposal",
			path:       pathProposalApplyAgree,
			method:     methodProposalApplyAgree,
			handler:    handler.ProposalApplyAgree,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "add a feedback",
			path:       pathFeedback,
			method:     methodFeedbackAdd,
			handler:    handler.AddFeedback,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "add a result/apply a proposal",
			path:       pathResult,
			method:     methodResultAdd,
			handler:    handler.AddResult,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "add a review",
			path:       pathReview,
			method:     methodReviewAdd,
			handler:    handler.AddReview,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "get reviews",
			path:       pathReview,
			method:     methodReviewSearch,
			handler:    handler.SearchReview,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "get topic list",
			path:       pathOrderList,
			method:     methodOrderList,
			handler:    handler.GetOrderList,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "get order by topic_id",
			path:       pathOrderTopic,
			method:     methodOrderTopicList,
			handler:    handler.GetOrderListByTopicID,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "get order by search title",
			path:       pathOrderBySearch,
			method:     methodOrderBySearch,
			handler:    handler.GetOrderBySearch,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "get user's apply order",
			path:       pathUserOrderApply,
			method:     methodUserOrderApply,
			handler:    handler.GetUserApplyOrder,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "get user's push order info ",
			path:       pathUserOrderInfo,
			method:     methodUserOrderInfo,
			handler:    handler.GetUserOrderInfo,
			middleware: middleware.JWTAuthMiddleware(),
		},
		{
			name:       "get order's info by order id",
			path:       pathOrder,
			method:     methodOrderSearch,
			handler:    handler.SearchOrder,
			middleware: middleware.JWTAuthMiddleware(),
		},
	}
)

// Init 初始化路由
func Init() {
	r := gin.Default()

	for _, route := range routes {
		if route.middleware == nil {
			r.Handle(route.method, route.path, route.handler)
		} else {
			r.Handle(route.method, route.path, route.middleware, route.handler)
		}
	}

	logrus.Println(r.Run())
}
