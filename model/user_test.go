// @program:     debug
// @file:        user_test.go.go
// @author:      edte
// @create:      2021-04-03 21:05
// @description:
package model

import (
	"reflect"
	"testing"
)

func TestAddUser(t *testing.T) {
	type args struct {
		p User
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "1",
			args: args{
				p: User{
					UserID:   "1234",
					Nickname: "ahsd",
					TopicID:  "sadf",
					Avatar:   "asdf",
					Username: "asdf",
					Password: "w3r",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := AddUser(tt.args.p); (err != nil) != tt.wantErr {
				t.Errorf("AddUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDeleteUser(t *testing.T) {
	type args struct {
		userID string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "1",
			args: args{
				userID: "1234",
			},
			wantErr: false,
		},
		{
			name: "2",
			args: args{
				userID: "123423423",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := DeleteUser(tt.args.userID); (err != nil) != tt.wantErr {
				t.Errorf("DeleteUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSearchUser(t *testing.T) {
	type args struct {
		userID string
	}
	tests := []struct {
		name    string
		args    args
		wantP   User
		wantErr bool
	}{
		{
			name: "",
			args: args{
				userID: "1sdfsd234",
			},
			wantP:   User{},
			wantErr: true,
		},
		{
			name: "",
			args: args{
				userID: "1234",
			},
			wantP: User{
				UserID:   "1234",
				Nickname: "ahsd",
				TopicID:  "sadf",
				Avatar:   "asdf",
				Username: "asdf",
				Password: "w3r",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotP, err := SearchUser(tt.args.userID)
			if (err != nil) != tt.wantErr {
				t.Errorf("SearchUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotP, tt.wantP) {
				t.Errorf("SearchUser() gotP = %v, want %v", gotP, tt.wantP)
			}
		})
	}
}

func TestUpdateUser(t *testing.T) {
	type args struct {
		p User
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "",
			args: args{
				p: User{
					UserID:   "1234",
					Nickname: "aaa",
					TopicID:  "aaa",
					Avatar:   "aaa",
					Username: "aaa",
					Password: "aaa",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := UpdateUser(tt.args.p.UserID, tt.args.p); (err != nil) != tt.wantErr {
				t.Errorf("UpdateUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserExist(t *testing.T) {
	type args struct {
		userID string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "",
			args: args{
				userID: "oi23josfd",
			},
			want: false,
		},
		{
			name: "",
			args: args{
				userID: "aaa",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ExistUser(tt.args.userID); got != tt.want {
				t.Errorf("UserExist() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPassWordRight(t *testing.T) {
	type args struct {
		username string
		password string
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "",
			args: args{
				username: "root",
				password: "root",
			},
			want:    true,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := PassWordRight(tt.args.username, tt.args.password)
			if (err != nil) != tt.wantErr {
				t.Errorf("PassWordRight() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("PassWordRight() got = %v, want %v", got, tt.want)
			}
		})
	}
}
