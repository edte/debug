// @program:     debug
// @file:        order.go
// @author:      edte
// @create:      2021-04-03 18:50
// @description:
package model

import (
	"github.com/jinzhu/gorm"

	"debug/errors"
)

// 订单
type Order struct {
	gorm.Model
	OrderID string // 订单 id
	UserID  string // 发出订单的用户
	TopicID string // 订单所属分区
	Title   string // 订单标题
	Price   string // 价格
	Expire  string // ddl
	Content string // 内容
}

func AddOrder(p Order) (err error) {
	if err := db.Create(&p).Error; err != nil {
		return errors.WarpModel("add order failed", err)
	}
	return nil
}

func UpdateOrder(p Order) (err error) {
	if err := db.Model(&p).Where("order_id = ?", p.OrderID).Update(&p).Error; err != nil {
		return errors.WarpModel("update order failed", err)
	}
	return nil
}

func DeleteOrder(orderID string) (err error) {
	if err := db.Where("order_id=?", orderID).Delete(&Order{}).Error; err != nil {
		return errors.WarpModel("delete order failed", err)
	}
	return nil
}

func SearchOrder(orderID string) (p Order, err error) {
	if err = db.Where("order_id = ?", orderID).First(&p).Error; err != nil {
		return p, errors.WarpModel("search order failed", err)
	}
	return
}

func SearchOrderByUid(uid string) (p []Order, err error) {
	if err = db.Where("user_id = ?", uid).Find(&p).Error; err != nil {
		return p, errors.WarpModel("search order by uid failed", err)
	}
	return
}

func ExistOrder(orderID string) bool {
	var u Order
	err := db.Where("order_id = ?", orderID).First(&u).Error
	return !errors.Is(err, gorm.ErrRecordNotFound)
}

func SearchOrderByTime() (p []Order, err error) {
	if err = db.Model(&Order{}).Order("created_at DESC").Limit(10).Find(&p).Error; err != nil {
		return nil, errors.WarpModel("search order by tim failed", err)
	}

	return
}

func SearchOrderByTopic(topicID string) (p []Order, err error) {
	if err = db.Model(&Order{}).Where("topic_id = ?", topicID).Order("created_at DESC").Limit(10).Find(&p).Error; err != nil {
		return nil, errors.WarpModel("search order by tim failed", err)
	}
	return
}

func SearchOrderByTitle(key string) (p []Order, err error) {
	if err = db.Model(&Order{}).Where("title LIKE ?", "%"+key+"%").Order("created_at DESC").Limit(10).Find(&p).Error; err != nil {
		return nil, errors.WarpModel("search order by tim failed", err)
	}
	return
}

func SearchOrderByContent(key string) (p []Order, err error) {
	if err = db.Model(&Order{}).Where("content LIKE ?", "%"+key+"%").Order("created_at DESC").Limit(10).Find(&p).Error; err != nil {
		return nil, errors.WarpModel("search order by tim failed", err)
	}
	return
}
