// @program:     debug
// @file:        topic.go
// @author:      edte
// @create:      2021-04-03 18:26
// @description:
package model

import (
	"github.com/jinzhu/gorm"

	"debug/errors"
)

// 分区
type Topic struct {
	gorm.Model
	TopicID string // 分区 id
	Name    string // 分区名称
	Icon    string // 分区图标 uri
}

func AddTopic(p Topic) (err error) {
	if err := db.Create(&p).Error; err != nil {
		return errors.WarpModel("add topic failed", err)
	}
	return nil
}

func UpdateTopic(p Topic) (err error) {
	if err := db.Model(&p).Where("topic_id = ?", p.TopicID).Update(&p).Error; err != nil {
		return errors.WarpModel("update topic failed", err)
	}
	return nil
}

func DeleteTopic(topicID string) (err error) {
	if err := db.Where("topic_id=?", topicID).Delete(&Topic{}).Error; err != nil {
		return errors.WarpModel("delete topic failed", err)
	}
	return nil
}

func SearchTopic(topicID string) (p Topic, err error) {
	if err = db.Where("topic_id = ?", topicID).First(&p).Error; err != nil {
		return p, errors.WarpModel("search topic failed", err)
	}
	return
}
