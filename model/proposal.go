// @program:     debug
// @file:        proposal.go
// @author:      edte
// @create:      2021-04-03 18:41
// @description:
package model

import (
	"github.com/jinzhu/gorm"

	"debug/errors"
)

// 对订单的接受请求
type Proposal struct {
	gorm.Model
	ProposalID   string
	UserID       string  // 发出请求的用户
	OrderID      string  // 订单
	Price        float64 // 价格
	EstimateTime string  // 预计完成时间
}

func AddProposal(p Proposal) (err error) {
	if err := db.Create(&p).Error; err != nil {
		return errors.WarpModel("add proposal failed", err)
	}
	return nil
}

func UpdateProposal(p Proposal) (err error) {
	if err := db.Model(&p).Where("proposal_id = ?", p.ProposalID).Update(&p).Error; err != nil {
		return errors.WarpModel("update proposal failed", err)
	}
	return nil
}

func DeleteProposal(proposalID string) (err error) {
	if err := db.Where("proposal_id=?", proposalID).Delete(&Proposal{}).Error; err != nil {
		return errors.WarpModel("delete proposal failed", err)
	}
	return nil
}

func SearchProposal(proposalID string) (p Proposal, err error) {
	if err = db.Where("proposal_id = ?", proposalID).First(&p).Error; err != nil {
		return p, errors.WarpModel("search proposal failed", err)
	}
	return
}

func ExistProposal(proposalID string) bool {
	var u Proposal
	err := db.Where("proposal_id = ?", proposalID).First(&u).Error
	return !errors.Is(err, gorm.ErrRecordNotFound)
}

func IsUsersProposal(uid, pid string) bool {
	var u Proposal
	err := db.Where("proposal_id = ?", pid).First(&u).Error
	if err != nil {
		return !errors.Is(err, gorm.ErrRecordNotFound)
	}
	return u.UserID == uid
}

func SearchProposalByUid(uid string) (p []Proposal, err error) {
	if err = db.Model(&Proposal{}).Where("user_id = ?", uid).Order("created_at DESC").Limit(10).Find(&p).Error; err != nil {
		return nil, errors.WarpModel("search order by uid failed", err)
	}
	return
}

func SearchProposalByOrderID(orderID string) (p []Proposal, err error) {
	if err = db.Model(&Proposal{}).Where("order_id = ?", orderID).Order("created_at DESC").Find(&p).Error; err != nil {
		return nil, errors.WarpModel("search order by uid failed", err)
	}
	return
}
