// @program:     debug
// @file:        review.go
// @author:      edte
// @create:      2021-04-03 18:28
// @description:
package model

import (
	"github.com/jinzhu/gorm"

	"debug/errors"
)

// 评价
type Review struct {
	gorm.Model
	ReviewID       string // 评价 id
	UserID         string // 评价的用户 id
	ReviewedUserID string // 被评价的用户 id
	ProposalID     string // 申请 id
	Data           string // 评价的数据
}

func AddReview(p Review) (err error) {
	if err := db.Create(&p).Error; err != nil {
		return errors.WarpModel("add review failed", err)
	}
	return nil
}

func UpdateReview(p Review) (err error) {
	if err := db.Model(&p).Where("review_id = ?", p.ReviewID).Update(&p).Error; err != nil {
		return errors.WarpModel("update review failed", err)
	}
	return nil
}

func DeleteReview(reviewID string) (err error) {
	if err := db.Where("review_id=?", reviewID).Delete(&Review{}).Error; err != nil {
		return errors.WarpModel("delete review failed", err)
	}
	return nil
}

func SearchReview(uid string) (p []Review, err error) {
	if err = db.Where("user_id = ?", uid).Find(&p).Error; err != nil {
		return p, errors.WarpModel("search review failed", err)
	}
	return
}

func ExistReview(proposalID string) bool {
	var r Review
	err := db.Where("proposal_id = ?", proposalID).First(&r).Error
	return !errors.Is(err, gorm.ErrRecordNotFound)
}
