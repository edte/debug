// @program:     debug
// @file:        user.go
// @author:      edte
// @create:      2021-04-03 18:18
// @description:
package model

import (
	"github.com/jinzhu/gorm"

	"debug/errors"
)

// 用户
type User struct {
	gorm.Model
	UserID   string // 用户 id
	Nickname string // 用户昵称
	TopicID  string // debug 默认分区
	Avatar   string // 用户头像
	Username string // 账号
	Password string // 密码
	Money    *int   `gorm:"default:100"`
}

func AddUser(p User) (err error) {
	if err := db.Create(&p).Error; err != nil {
		return errors.WarpModel("add user failed", err)
	}
	return nil
}

func UpdateUser(uid string, p User) (err error) {
	if err := db.Model(&p).Where("user_id = ?", uid).Update(p).Error; err != nil {
		return errors.WarpModel("update user failed", err)
	}
	return nil
}

func DeleteUser(userID string) (err error) {
	if err := db.Where("user_id=?", userID).Delete(&User{}).Error; err != nil {
		return errors.WarpModel("delete user failed", err)
	}
	return nil
}

func SearchUser(userID string) (p User, err error) {
	if err = db.Where("user_id = ?", userID).First(&p).Error; err != nil {
		return p, errors.WarpModel("search user failed", err)
	}
	return
}

func SearchUserByUsername(username string) (p User, err error) {
	if err = db.Where("username = ?", username).First(&p).Error; err != nil {
		return p, errors.WarpModel("search user failed", err)
	}
	return
}

func ExistUser(username string) bool {
	var u User
	err := db.Where("username = ?", username).First(&u).Error
	return !errors.Is(err, gorm.ErrRecordNotFound)
}

func PassWordRight(username string, password string) (bool, error) {
	var u User
	err := db.Where("username = ?", username).First(&u).Error
	if err != nil {
		return false, errors.WarpModel("judge password whether right failed", err)
	}

	return u.Password == password, nil
}
