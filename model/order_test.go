// @program:     debug
// @file:        order_test.go.go
// @author:      edte
// @create:      2021-04-12 11:42
// @description:
package model

import (
	"reflect"
	"testing"
)

func TestSearchOrderByTime(t *testing.T) {
	tests := []struct {
		name    string
		wantP   []Order
		wantErr bool
	}{
		{
			name:    "",
			wantP:   nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotP, err := SearchOrderByTime()
			if (err != nil) != tt.wantErr {
				t.Errorf("SearchOrderByTime() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotP, tt.wantP) {
				t.Errorf("SearchOrderByTime() gotP = %v, want %v", gotP, tt.wantP)
			}
		})
	}
}

func TestSearchOrderByTopic(t *testing.T) {
	type args struct {
		topicID string
	}
	tests := []struct {
		name    string
		args    args
		wantP   []Order
		wantErr bool
	}{
		{
			name: "",
			args: args{
				topicID: "asdf",
			},
			wantP:   nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotP, err := SearchOrderByTopic(tt.args.topicID)
			if (err != nil) != tt.wantErr {
				t.Errorf("SearchOrderByTopic() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotP, tt.wantP) {
				t.Errorf("SearchOrderByTopic() gotP = %v, want %v", gotP, tt.wantP)
			}
		})
	}
}
