// @program:     debug
// @file:        result.go
// @author:      edte
// @create:      2021-04-03 18:37
// @description:
package model

import (
	"github.com/jinzhu/gorm"

	"debug/errors"
)

const (
	Accepting = iota // 正在接受中,默认
	Applyed          // 接受
	Delay            // 拒绝
	Succeed          // 成功
	Overdue          // 过期
)

// 对每次接受订单的结果
type Result struct {
	gorm.Model
	Type       int    // 结果类型
	ResultID   string // 结果 id
	OrderID    string // 订单 id
	ProposalID string // 申请 id
}

func AddResult(p Result) (err error) {
	if err := db.Create(&p).Error; err != nil {
		return errors.WarpModel("add result failed", err)
	}
	return nil
}

func UpdateResult(p Result) (err error) {
	if err := db.Model(&p).Where("result_id = ?", p.ResultID).Update(&p).Error; err != nil {
		return errors.WarpModel("update result failed", err)
	}
	return nil
}

func DeleteResult(resultID string) (err error) {
	if err := db.Where("result_id=?", resultID).Delete(&Result{}).Error; err != nil {
		return errors.WarpModel("delete result failed", err)
	}
	return nil
}

func SearchResult(proposalID string) (i int, err error) {
	var r Result
	if err := db.Where("proposal_id = ?", proposalID).First(&r).Error; err != nil {
		return 0, errors.WarpModel("search result failed", err)
	}
	return r.Type, nil
}

func SearchResultByOrderID(orderID string) (i int, err error) {
	var r Result
	err = db.Where("order_id = ?", orderID).First(&r).Error

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return 0, nil
	}

	if err != nil {
		return 0, errors.WarpModel("search result by order id fiale", err)
	}
	return r.Type, nil
}

func IsSucceedOrder(orderID string) (bool, string, error) {
	var r Result
	err := db.Select("proposal_id").Where("type = 1 AND order_id = ?", orderID).First(&r).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return false, "", nil
	}
	if err != nil {
		return false, "", errors.WarpModel("failed to judge order if is succeed", err)
	}

	return true, r.ProposalID, nil
}

func AbleResultType(t int) bool {
	return t == Applyed || t == Delay
}
