// @program:     debug
// @file:        review_test.go.go
// @author:      edte
// @create:      2021-04-11 22:16
// @description:
package model

import (
	"testing"
)

func TestExistReview(t *testing.T) {
	type args struct {
		proposalID string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "",
			args: args{
				proposalID: "asdf",
			},
			want: false,
		},
		{
			name: "",
			args: args{
				proposalID: "923MCxvz",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ExistReview(tt.args.proposalID); got != tt.want {
				t.Errorf("ExistReview() = %v, want %v", got, tt.want)
			}
		})
	}
}
