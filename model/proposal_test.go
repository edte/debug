// @program:     debug
// @file:        proposal_test.go.go
// @author:      edte
// @create:      2021-04-11 21:25
// @description:
package model

import (
	"reflect"
	"testing"
)

func TestIsUsersProposal(t *testing.T) {
	type args struct {
		uid string
		pid string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "",
			args: args{
				uid: "124",
				pid: "923MCxvz",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsUsersProposal(tt.args.uid, tt.args.pid); got != tt.want {
				t.Errorf("IsUsersProposal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExistProposal(t *testing.T) {
	type args struct {
		proposalID string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "",
			args: args{
				proposalID: "923MCxvz",
			},
			want: true,
		},
		{
			name: "",
			args: args{
				proposalID: "sdf",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ExistProposal(tt.args.proposalID); got != tt.want {
				t.Errorf("ExistProposal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSearchProposalByUid(t *testing.T) {
	type args struct {
		uid string
	}
	tests := []struct {
		name    string
		args    args
		wantP   []Proposal
		wantErr bool
	}{
		{
			name: "",
			args: args{
				uid: "2341234",
			},
			wantP:   nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotP, err := SearchProposalByUid(tt.args.uid)
			if (err != nil) != tt.wantErr {
				t.Errorf("SearchProposalByUid() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotP, tt.wantP) {
				t.Errorf("SearchProposalByUid() gotP = %v, want %v", gotP, tt.wantP)
			}
		})
	}
}
