// @program:     debug
// @file:        table.go
// @author:      edte
// @create:      2021-04-03 18:15
// @description:
package model

var (
	tables = []struct {
		name string
		data interface{}
	}{
		{
			name: "1",
			data: &User{},
		},
		{
			name: "2",
			data: &Topic{},
		},
		{
			name: "3",
			data: &Review{},
		},
		{
			name: "4",
			data: &Feedback{},
		},
		{
			name: "5",
			data: &Order{},
		},
		{
			name: "6",
			data: &Proposal{},
		},
		{
			name: "7",
			data: &Result{},
		},
	}
)

// initTable 初始化 table 结构
func initTable() {
	for _, v := range tables {
		if db.HasTable(v.data) {
			db.AutoMigrate(v.data)
		} else {
			db.CreateTable(v.data)
		}
	}
}
