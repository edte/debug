// @program:     debug
// @file:        db.go
// @author:      edte
// @create:      2021-04-03 18:07
// @description:
package model

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
)

var (
	db *gorm.DB
)

// connectDb 连接 mysql
func connectDb() {
	d, err := gorm.Open("mysql", fmt.Sprintf("%s:%s@(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", "root", "mima", "127.0.0.1", "3306", "debug"))
	if err != nil {
		logrus.Fatalf("failed to connect database:%v", err)
	}

	db = d

	// 查看原始 sql
	// db.LogMode(true)
}
