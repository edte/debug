// @program:     debug
// @file:        feedback.go
// @author:      edte
// @create:      2021-04-03 18:57
// @description:
package model

import (
	"github.com/jinzhu/gorm"

	"debug/errors"
)

// Feedback 是用户对订单的反馈
type Feedback struct {
	gorm.Model
	FeedbackID string // 反馈 id
	UserID     string // 反馈的用户
	OrderID    string // 反馈的订单
	Type       int    // 反馈的类型
	Data       string // 反馈的数据
}

func AddFeedback(p Feedback) (err error) {
	if err := db.Create(&p).Error; err != nil {
		return errors.WarpModel("add feedback failed", err)
	}
	return nil
}

func UpdateFeedback(p Feedback) (err error) {
	if err := db.Model(&p).Where("feedback_id = ?", p.FeedbackID).Update(&p).Error; err != nil {
		return errors.WarpModel("update feedback failed", err)
	}
	return nil
}

func DeleteFeedback(feedbackID string) (err error) {
	if err := db.Where("feedback_id=?", feedbackID).Delete(&Feedback{}).Error; err != nil {
		return errors.WarpModel("delete feedback failed", err)
	}
	return nil
}

func SearchFeedback(feedbackID string) (p Feedback, err error) {
	if err = db.Where("feedback_id = ?", feedbackID).First(&p).Error; err != nil {
		return p, errors.WarpModel("search feedback failed", err)
	}
	return
}
