// @program:     debug
// @file:        init.go
// @author:      edte
// @create:      2021-04-03 18:06
// @description:
package model

// init 初始化 mysql
func init() {
	connectDb()
	initTable()
}
