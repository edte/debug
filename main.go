// @program:     debug
// @file:        main.go
// @author:      edte
// @create:      2021-03-30 21:59
// @description:
package main

import (
	"debug/router"
)

func main() {
	router.Init()
}
