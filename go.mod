module debug

go 1.15

require (
	github.com/appleboy/gin-jwt v2.5.0+incompatible // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jinzhu/gorm v1.9.16
	github.com/sirupsen/logrus v1.8.1
	gopkg.in/dgrijalva/jwt-go.v3 v3.2.0
)
