// @program:     debug
// @file:        errors_test.go.go
// @author:      edte
// @create:      2021-04-05 15:49
// @description:
package errors

import (
	"errors"
	"reflect"
	"testing"
)

func TestNew(t *testing.T) {
	type args struct {
		t       layer
		message string
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			name: "",
			args: args{
				t:       Model,
				message: "add user failed",
			},
			wantErr: errors.New("[model]: add user failed"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := New(tt.args.t, tt.args.message); err.Error() != tt.wantErr.Error() {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUnwarp(t *testing.T) {
	type args struct {
		err error
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Unwarp(tt.args.err); (err != nil) != tt.wantErr {
				t.Errorf("Unwarp() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestWarp(t *testing.T) {
	type args struct {
		t   layer
		s   string
		err error
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			name: "",
			args: args{
				t:   Model,
				s:   "add user failed",
				err: newRawError(Model, "column not found"),
			},
			wantErr: errors.New("[model]: add user failed 》 [model]: column not found"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Warp(tt.args.t, tt.args.s, tt.args.err); err.Error() != tt.wantErr.Error() {
				t.Errorf("Warp() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestWarpModel(t *testing.T) {
	type args struct {
		s   string
		err error
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := WarpModel(tt.args.s, tt.args.err); (err != nil) != tt.wantErr {
				t.Errorf("WarpModel() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_newRawError(t *testing.T) {
	type args struct {
		t       layer
		message string
	}
	tests := []struct {
		name string
		args args
		want *rawError
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newRawError(tt.args.t, tt.args.message); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("newRawError() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_rawError_Error(t *testing.T) {
	type fields struct {
		t       layer
		message string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := &rawError{
				t:       tt.fields.t,
				message: tt.fields.message,
			}
			if got := e.Error(); got != tt.want {
				t.Errorf("Error() = %v, want %v", got, tt.want)
			}
		})
	}
}
