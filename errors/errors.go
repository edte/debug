// @program:     debug
// @file:        errors.go
// @author:      edte
// @create:      2021-04-03 18:08
// @description:
package errors

import (
	"errors"
	"fmt"
)

type layer string

const (
	Model   layer = "model"
	Service       = "service"
	Router        = "router"
	Config        = "config"
	UnKnown       = "unknown"
)

type rawError struct {
	t       layer
	message string
}

func newRawError(t layer, message string) *rawError {
	return &rawError{
		t:       t,
		message: message,
	}
}

func (e *rawError) Error() string {
	return fmt.Sprintf("[%v]: %s", e.t, e.message)
}

func New(t layer, message string) error {
	return newRawError(t, message)
}

func Raw(s string) error {
	return errors.New(s)
}

func Warp(t layer, s string, err error) error {
	return fmt.Errorf("[%v]: %s 》 %w", t, s, err)
}

// 每个函数调用这个函数后，只需要说明当前函数中失败的行为，而不要管调用的其他函数，或者这个函数会被其他函数调用的事
func WarpModel(s string, err error) error {
	return fmt.Errorf("[%v]: %s 》 %w", Model, s, err)
}

func WarpRouter(s string, err error) error {
	return fmt.Errorf("[%v]: %s 》 %w", Router, s, err)
}

func Unwarp(err error) error {
	return errors.Unwrap(err)
}

func Is(err, target error) bool {
	return errors.Is(err, target)
}
